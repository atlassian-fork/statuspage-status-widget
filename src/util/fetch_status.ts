import 'whatwg-fetch';
import { Summary } from '../types/summary';
import { rejects } from 'assert';

const CACHE: {
  [url: string]: {
    expires: number;
    data: Promise<Summary>;
  };
} = {};

function fetchCache(path: string): (base_url: string) => Promise<Summary> {
  return function(base_url: string): Promise<Summary> {
    const url = new URL(base_url);
    url.pathname = path;

    if (!CACHE[url.toString()] || CACHE[url.toString()].expires < Date.now()) {
      CACHE[url.toString()] = {
        expires: Date.now() + 30000, // 30 seconds
        data: fetch(url.toString()).then(response => {
          if (!response.ok) {
            throw new Error(response.statusText);
          }
          return response.json();
        }),
      };
    }

    return CACHE[url.toString()].data;
  };
}

export const fetchSummary = fetchCache('/api/v2/summary.json');
